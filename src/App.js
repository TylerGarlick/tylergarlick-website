import React from 'react';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from './components/Header';
import Footer from './components/Footer';

import HomePage from './pages/Home';
import AboutPage from './pages/About';
import NotFound from './pages/NotFound';

import './index.css';

export default () => (
  <Router>
    <main>
      <Header title="Tyler Garlick" />
      <article>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/about" component={AboutPage} />
          <Route component={NotFound} />
        </Switch>
      </article>
      <Footer />
    </main>
  </Router>
);

import React from 'react';
import AppBar from 'material-ui/AppBar';
import './Header.css';
//
// export default class extends Component {
//
//   constructor(props) {
//     super(props);
//     console.log(props);
//   }
//
//   render() {
//     return (
//       <header>
//         <h2><a href="/">{this.props.title}</a></h2>
//         <nav>
//           <a href="/about">About</a>
//           <a href="#">Blog</a>
//         </nav>
//       </header>
//     );
//   }
// }

export default (props) => (
  <AppBar title={props.title} iconClassNameRight="muidocs-icon-navigation-expand-more" />
)

// <header>
//   <h2><a href="/">{props.title}</a></h2>
//   <nav>
//     <a href="/about">About</a>
//     <a href="#">Blog</a>
//   </nav>
// </header>
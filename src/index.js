import React from 'react';
import ReactDOM from 'react-dom';

import TapPlugin from 'react-tap-event-plugin';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import LightTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import App from './App';
import registerServiceWorker from './registerServiceWorker';


const Wrapped = (
  <MuiThemeProvider muiTheme={getMuiTheme(LightTheme)}>
    <App />
  </MuiThemeProvider>
);

ReactDOM.render(Wrapped, document.getElementById('root'));

TapPlugin();
registerServiceWorker();
